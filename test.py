import unittest 
import prime

class TestPrime(unittest.TestCase):

    

    def test_circulate(self):
        number = "197"
        # Expectation:
        exp = "971"
        # Actual result
        act = prime.circulate(number)

        self.assertEqual(exp,act)



    def test_isCircularPrime(self):
        number = 11
        # Expectation:
        exp = True
        # Actual result
        act = prime.isCircularPrime(number)

        self.assertEqual(exp,act)    


    def test_prime_list(self):
        number = 20
        # Expectation:
        exp = [ 2, 3, 5, 7, 11, 13, 17, 19]
        # Actual result
        act = prime.prime_list(number)

        self.assertEqual(exp,act)    

    
    def test_main(self):
        number = 101
        # Expectation:
        exp = [ 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79,97]
        # Actual result
        act = prime.main(number)

        self.assertEqual(exp,act)    



if '__name__' == '__main__':
    unittest.main()