import sympy 

def prime_list(number):
    arr = []
    for n in range(number):
        if(sympy.isprime(n)):
            arr.append(n)
    return arr



# will rotate the array 
def circulate(n): #O(1)
    s = n
    s = s[1:] + s[0]
    return s


# check if the number circular prime or not
def isCircularPrime(n): #O(n log n)
    a = str(n)
    while(sympy.isprime(int(a))):
        a  = circulate(a)
        if(a == str(n)):
            return True
    return False



# return circular prime numbers from 0 to n 
def main(n):  #O(n log n)
    # get array of prime numbers 
    arr =  prime_list(n)
    circularPrimeList = [] 
    for a in arr:
        if(isCircularPrime(a)):
            circularPrimeList.append(a)
    return circularPrimeList


print(main(1000000))


#  primeNumbers(givenNumber):     O(n^2 log n)
#  isPrime(n):                    O(n log n)
#  circulate(n):                  O(1)
#  isCircularPrime(n):            O(n log n)
#  main(n):                       O(n log n)
# 
#  Total: O(n^5 log n^4) => O (4 n^5 log n)
